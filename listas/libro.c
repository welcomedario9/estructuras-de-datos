// Estructuras de Datos
// libro.c 

// Librerías
#include <stdio.h>
#include <string.h>
#include "libro.h"

// Pide al usuario la información de una cantidad determinada de libros
// y los llena.
Libro LlenarLibro(Libro libro) {
    __fpurge(stdin);    // limpia el buffer

    printf("Ingrese el titulo del libro: ");
    fgets(libro.titulo, 50, stdin);
    libro.titulo[strlen(libro.titulo)-1] = '\0';
    printf("Ingrese el autor del libro: ");
    fgets(libro.autor, 50, stdin);
    libro.autor[strlen(libro.autor)-1] = '\0';
    printf("Ingrese el isbn del libro: ");
    fgets(libro.isbn, 13, stdin);
    libro.isbn[strlen(libro.isbn)-1] = '\0';
    return libro;
}
