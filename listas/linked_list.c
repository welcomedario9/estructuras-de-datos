// Estructuras de datos
// Ejemplo practico y sencillo del manejo de las lista enlazadas vesión 2
// linked_list.c

// librerías
#include <stdio.h>
#include <stdlib.h>

// estructuras 
typedef struct dato {
    char nombre[10];
    char telefono[10];
    struct dato *siguiente;
} Registro;


// principal
int main() {
    
    // variables
    Registro *lista = NULL;
    
    int i = 0, salir = 0;

    while (salir < 2) {
        Registro *nuevoregistro, *auxiliar;
        nuevoregistro = (Registro *) malloc(sizeof(Registro));
        printf("Ingrese un nombre: ");
        gets(nuevoregistro->nombre);
        printf("Ingrese el telefono: ");
        gets(nuevoregistro->telefono);
        nuevoregistro->siguiente = NULL;

        if (lista == NULL) {
            lista = nuevoregistro;
        } else {
            auxiliar = lista;
            while (auxiliar->siguiente) {
                auxiliar = auxiliar->siguiente;
            }
            auxiliar->siguiente = nuevoregistro;
        }
        salir++;
    }

    while (lista) {
        printf("Nombre: %s\t", lista->nombre);
        printf("Telefono: %s\n", lista->telefono);
        lista = lista->siguiente;
    }    
        

    return 0;
}
